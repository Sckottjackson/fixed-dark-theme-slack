### There is a cool [Slack dark theme](https://github.com/Nockiro/slack-black-theme), but on Mojave Mac showed up some styles problems, which fixed by myself
### Paste code below into ssb-interop.js which located at ...
### / Applications / Slack.app / Contents / Resources / app.asar.unpacked / src / static / ssb-interop.js

#

```javascript
// First make sure the wrapper app is loaded
document.addEventListener("DOMContentLoaded", function() {

    // Then get its webviews
    let webviews = document.querySelectorAll(".TeamView webview");
 
    // Fetch our CSS in parallel ahead of time
    const cssPath = 'https://bitbucket.org/Sckottjackson/customcssslack/raw/b86febe6b99b6825b925e18d4a557cde45217ab1/styles.css';
    let cssPromise = fetch(cssPath).then(response => response.text());
 
    let customCustomCSS = `
    :root {
       /* Modify these to change your theme colors: */
       --primary: #168394;
       --text: #CCC;
       --text-special: #21a9c7;
       --background: #282C34;
       --background-elevated: #3B4048;
    }
    `
 
    // Insert a style tag into the wrapper view
    cssPromise.then(css => {
       let s = document.createElement('style');
       s.type = 'text/css';
       s.innerHTML = css + customCustomCSS;
       document.head.appendChild(s);
    });
 
    // Wait for each webview to load
    webviews.forEach(webview => {
       webview.addEventListener('ipc-message', message => {
          if (message.channel == 'didFinishLoading')
             // Finally add the CSS into the webview
             cssPromise.then(css => {
                let script = `
                      let s = document.createElement('style');
                      s.type = 'text/css';
                      s.id = 'slack-custom-css';
                      s.innerHTML = \`${css + customCustomCSS}\`;
                      document.head.appendChild(s);
                      `
                webview.executeJavaScript(script);
             })
       });
    });
 });
```